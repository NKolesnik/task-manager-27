package ru.t1consulting.nkolesnik.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.exception.field.DateIncorrectException;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("[ENTER DATE BEGIN:]");
        @Nullable final Date dateBegin = TerminalUtil.nextDate();
        if (dateBegin == null) throw new DateIncorrectException();
        System.out.println("[ENTER DATE END:]");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        if (dateEnd == null) throw new DateIncorrectException();
        @NotNull final String userId = getUserId();
        getProjectService().create(userId, name, description, dateBegin, dateEnd);
    }

}
