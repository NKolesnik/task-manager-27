package ru.t1consulting.nkolesnik.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-change-password";

    @NotNull
    public static final String DESCRIPTION = "Change user password.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getAuthService().getUserId();
        System.out.println("[ENTER NEW PASSWORD]");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        getUserService().setPassword(userId, newPassword);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
